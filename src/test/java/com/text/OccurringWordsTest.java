package com.text;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class OccurringWordsTest {

    @Test
    public void whenTopThreeWordsIsCalledByNullStringThenExpectEmptyArray() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = null;
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList();
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void whenTopThreeWordsIsCalledByEmptyStringThenExpectEmptyArray() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = "";
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList();
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void whenTopThreeWordsIsCalledByOneCapitalWordThenExpectOneElementInArray() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = "Wont";
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList("wont");
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void whenTopThreeWordsIsCalledBySentenceWithTwoDifferentWordsThenExpectTwoWords() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = " //wont won't won't";
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList("won't", "wont");
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void whenTopThreeWordsIsCalledBySentenceWithRepeatedWordsThenExpectTop3Words() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = "e e e e DDD ddd DdD: ddd ddd aa aA Aa, bb cc cC e e e";
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList("e", "ddd", "aa");
        Assert.assertEquals(result, expectedResult);
    }

    @Test
    public void whenTopThreeWordsIsCalledBySentenceWithNewLinesAndRepeatedWordsThenExpectTop3Words() {
        OccurringWords occurringWords = new OccurringWords();
        String sentence = "In a village of La Mancha, the name of which I have\n" +
                "no desire to call to\n" +
                "mind, there lived not long since one of those gentlemen that keep a lance\n" +
                "in the lance-rack, an old buckler, a lean hack, and a greyhound for\n" +
                "coursing. An olla of rather more beef than mutton, a salad on most\n" +
                "nights, scraps on Saturdays, lentils on Fridays, and a pigeon or so extra\n" +
                "on Sundays, made away with three-quarters of his income.";
        final List<String> result = occurringWords.top_3_words(sentence);
        List<String> expectedResult = Arrays.asList("a", "of", "on");
        Assert.assertEquals(result, expectedResult);
    }

}
