package com.text;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static java.util.Map.Entry.comparingByValue;
import static java.util.stream.Collectors.toMap;


public class OccurringWords {

    private static final String ESCAPE_CHARACTER = "//";
    private static final String REGULAR_EXPRESSION_NEW_LINE = "[\\n]+";
    private static final String SPACE = " ";
    private static final String EMPTY_STRING = "";
    private static final int ONE = 1;

    public List<String> top_3_words(String sentence) {

        if (sentence == null || sentence.isEmpty()) {
            return Arrays.asList();
        }

        sentence = sentence.toLowerCase().replaceAll(REGULAR_EXPRESSION_NEW_LINE,SPACE).replace(ESCAPE_CHARACTER, EMPTY_STRING);
        Map<String, Integer> wordsWithNoOfCount = getMapOfWordWithNumberOfRepetitionOfEachWordInSentence(sentence.split(SPACE));
        Map<String, Integer> wordsCountInDescOrder = sortMapDescendingOrder(wordsWithNoOfCount);
        return wordsCountInDescOrder.keySet().stream().limit(3).collect(Collectors.toList());
    }

    private Map<String, Integer> getMapOfWordWithNumberOfRepetitionOfEachWordInSentence(String[] words) {
        Map<String, Integer> wordWithNoOfCount = new HashMap<>();
        int count = 0;

        IntStream.range(0, words.length).filter(i -> !words[i].isEmpty() && words[i] != null).forEach(i -> {
            if (!wordWithNoOfCount.isEmpty() && wordWithNoOfCount.containsKey(words[i])) {
                wordWithNoOfCount.put(words[i], wordWithNoOfCount.get(words[i]) + ONE);
            } else {
                wordWithNoOfCount.put(words[i], count);
            }
        });

        return wordWithNoOfCount;
    }

    private LinkedHashMap<String, Integer> sortMapDescendingOrder(Map<String, Integer> stringIntegerMap) {
        return stringIntegerMap
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }

}
